"""chargebee item utility module. Items are plans, addons and charges"""
from lib.chargebee.config import setted_chargebee


class Item:  # pylint: disable=too-many-instance-attributes
    """chargebee item as a python object
    https://apidocs.eu.chargebee.com/docs/api/items
    """

    def __init__(self):
        self.id: str
        self.name: str
        self.external_name: str
        self.description: str
        self.status: str
        self.resource_version: int
        self.updated_at: int
        self.item_family_id: str
        self.is_shippable: bool
        self.is_giftable: bool
        self.redirect_url: str
        self.enabled_for_checkout: bool
        self.enabled_in_portal: bool
        self.included_in_mrr: str
        self.item_applicability: str
        self.gift_claim_redirect_url: str
        self.unit: str
        self.metered: bool
        self.usage_calculation: str
        self.archived_at: int
        self.channel: str
        self.metadata: dict
        self.applicable_items: list[str]
        self.type: str
        self.object: str

    def __str__(self) -> str:
        return f"{self.type}: {self.name} ({self.id})"

    @staticmethod
    def init_from_dict(data: dict) -> "Item":
        """init Item from a dictionnary

        Args:
            data (dict): Item dict from chargebee.
        """
        item = Item()
        for attr_name, attr_value in data.items():
            setattr(item, attr_name, attr_value)
        return item

    @staticmethod
    def get_from_id(item_id: str) -> "Item":
        """get Item from chargebee id

        Args:
            id (str): chargebee item id.
        """
        values = setted_chargebee.Item.retrieve(item_id).item.values  # type: ignore
        return Item.init_from_dict(values)

    @staticmethod
    def get_items() -> list["Item"]:
        """get chargebee items

        Returns:
            list[Item]: a list of items (100 max)
        """
        items = setted_chargebee.Item.list({"limit": 100})
        return [Item.init_from_dict(item_dict["item"]) for item_dict in items.response]  # type: ignore


if __name__ == "__main__":
    for item_1 in Item.get_items():
        item_2 = Item.get_from_id(item_1.id)
        print(item_2)
