"""chargebee item price utility module"""
from lib.chargebee.config import setted_chargebee


class ItemPrice:  # pylint: disable=too-many-instance-attributes
    """chargebee item price as a python object
    https://apidocs.chargebee.com/docs/api/item_prices?lang=python
    """

    def __init__(self):
        self.id: str
        self.name: str
        self.item_family_id: str
        self.product_id: str
        self.item_id: str
        self.description: str
        self.status: str
        self.external_name: str
        self.pricing_model: str
        self.price: int
        self.price_in_decimal: str
        self.period: int
        self.currency_code: str
        self.period_unit: str
        self.trial_period: int
        self.trial_period_unit: str
        self.trial_end_action: str
        self.shipping_period: int
        self.shipping_period_unit: str
        self.billing_cycles: int
        self.free_quantity: int
        self.free_quantity_in_decimal: str
        self.channel: str
        self.resource_version: int
        self.updated_at: int
        self.created_at: int
        self.archived_at: int
        self.invoice_notes: str
        self.is_taxable: bool
        self.metadata: dict
        self.item_type: str
        self.show_description_in_invoices: bool
        self.show_description_in_quotes: bool
        self.tiers: list
        self.tax_detail: dict
        self.accounting_detail: dict
        self.object: str

    def __str__(self) -> str:
        return f"{self.id} ({self.name}: {self.price/100} {self.currency_code})\r\n"

    @staticmethod
    def init_from_dict(data: dict) -> "ItemPrice":
        """init ItemPrice from a dictionnary

        Args:
            data (dict): ItemPrice dict from chargebee.
        """
        item_price = ItemPrice()
        for attr_name, attr_value in data.items():
            setattr(item_price, attr_name, attr_value)
        return item_price

    @staticmethod
    def get_from_id(item_id: str) -> "ItemPrice":
        """get ItemPrice from chargebee id

        Args:
            id (str): chargebee item price id.
        """
        values = setted_chargebee.ItemPrice.retrieve(item_id).item_price.values  # type: ignore
        return ItemPrice.init_from_dict(values)

    @staticmethod
    def get_item_prices() -> list["ItemPrice"]:
        """get chargebee item prices

        Returns:
            list[ItemPrice]: a list of item prices (100 max)
        """
        items = setted_chargebee.ItemPrice.list({"limit": 100})
        return [ItemPrice.init_from_dict(item_dict["item_price"]) for item_dict in items.response]  # type: ignore


if __name__ == "__main__":
    for item_price_1 in ItemPrice.get_item_prices():
        item_price_2 = ItemPrice.get_from_id(item_price_1.id)
        print(item_price_2.metadata)
