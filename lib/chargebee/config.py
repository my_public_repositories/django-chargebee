"""chargebee config loader"""
import chargebee

from lib.config import EnvLoader

chargebee.configure(EnvLoader.CHARGEBEE_KEY, EnvLoader.CHARGEBEE_WEBSITE)

setted_chargebee = chargebee
