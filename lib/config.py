"""Config utility"""
import logging
import os

from dotenv import load_dotenv

load_dotenv()
logging.info("loading config")


class EnvLoader:
    """Environment variable loader"""

    CHARGEBEE_KEY = os.getenv("CHARGEBEE_KEY")
    CHARGEBEE_WEBSITE = os.getenv("CHARGEBEE_WEBSITE")
    NGROK_URL = os.getenv("NGROK_URL")
