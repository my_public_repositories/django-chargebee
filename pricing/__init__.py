import os

import django.conf
import django.db.utils

# load django settings for scripts if needed
if not django.conf.settings.configured:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "honeycomics.settings")
    django.setup()
