# pricing/views.py
import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import HttpResponseForbidden, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, ListView, View

from lib.chargebee.config import setted_chargebee

from .models import Plan, PlanPrice, Subscription


class PlanView(LoginRequiredMixin, ListView):
    model = Plan
    context_object_name = "plans"
    template_name = "pricing/plans.html"


class PlanDetailView(LoginRequiredMixin, DetailView):
    model = Plan
    context_object_name = "plan"
    template_name = "pricing/plan_detail.html"

    def get_context_data(self, **kwargs):
        context = super(PlanDetailView, self).get_context_data()
        context["plan_prices"] = PlanPrice.objects.filter(plan=self.get_object())
        return context


class CreateCBSubscriptionView(View):
    def post(self, request, *args, **kwargs):
        try:
            price_id = request.POST.get("price_id")

            # Use the received price_id in the checkout request
            result = setted_chargebee.HostedPage.checkout_new_for_items(
                {
                    "subscription_items": [
                        {"item_price_id": price_id},
                    ],
                    "customer": {
                        "first_name": request.user.first_name,
                        "last_name": request.user.last_name,
                        "email": request.user.email,
                    },
                }
            )

            # Extract hosted_page from the result
            hosted_page = result._response["hosted_page"]
            # Return the hosted_page data as JSON response
            return JsonResponse(hosted_page)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON data in the request."}, status=400)


# EU region IP addresses
ALLOWED_IPS = [
    "127.0.0.1",
    "18.159.27.9",
    "18.159.5.237",
    "3.126.22.116",
    "3.124.98.200",
    "18.158.188.144",
    "18.197.55.93",
    "18.158.1.51",
    "18.159.186.201",
    "18.159.83.75",
    "3.121.1.124",
    "52.28.49.9",
    "52.58.215.160",
]


@method_decorator(csrf_exempt, name="dispatch")
class WebhookView(View):
    def post(self, request, *args, **kwargs):
        try:
            remote_ip = request.META.get("REMOTE_ADDR", None)
            if remote_ip in ALLOWED_IPS:
                event = json.loads(request.body)
                if event["event_type"] == "subscription_created":
                    content = event["content"]
                    email = content["customer"]["email"]
                    user = User.objects.get(email=email)
                    item_price_id = content["subscription"]["subscription_items"][0]["item_price_id"]
                    item_price = PlanPrice.objects.get(id=item_price_id)
                    Subscription.objects.create(user=user, plan_price=item_price)
                    return JsonResponse({"status": "success"}, status=200)
                else:
                    print("Unhandled event type")
                    return JsonResponse({"status": "success"}, status=200)
            else:
                return HttpResponseForbidden("Access denied: Request from unauthorized IP.")
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=500)


class SubscriptionView(LoginRequiredMixin, ListView):
    model = Subscription
    template_name = "pricing/subscription.html"
    context_object_name = "subscription"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["subscription"] = Subscription.objects.filter(user=self.request.user).order_by("-created_at").first()
        return context
