from django.conf import settings
from django.db import models

from lib.chargebee.item import Item
from lib.chargebee.item_price import ItemPrice

# Create your models here.
# pricing/models.py


User = settings.AUTH_USER_MODEL


class Plan(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return self.name


def initialise_plan_list_from_chargebee():
    for item in Item.get_items():
        plan = Plan(id=item.id, name=item.name, description=item.description)
        plan.save()


class PlanPrice(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    price = models.IntegerField()
    currency = models.CharField(max_length=255, default="USD")
    period = models.CharField(max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f"{self.period} {self.plan.name} {self.currency} plan"


def initialise_plan_price_list_from_chargebee():
    for item_price in ItemPrice.get_item_prices():
        print(item_price.id)
        print(item_price.item_id)
        print(item_price.price)
        print(item_price.currency_code)
        print(item_price.period_unit)
        plan = Plan.objects.get(id=item_price.item_id)
        plan_price = PlanPrice(
            id=item_price.id,
            plan=plan,
            price=item_price.price,
            currency=item_price.currency_code,
            period=item_price.period_unit,
        )
        plan_price.save()


class Subscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    plan_price = models.ForeignKey(PlanPrice, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f"{self.user.username} {self.plan_price.plan.name}"
