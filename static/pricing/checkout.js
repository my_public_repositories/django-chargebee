$(document).ready(function () {
    Chargebee.init({ site: 'seer-cs-test' })
    let cbInstance = Chargebee.getInstance()
    $('.checkout-button').click(function () {
        let selectedPriceId = $(this).data('price')
        console.log(selectedPriceId)
        // const csrftoken = getCookie('csrftoken')
        let csrfTokenValue = document.querySelector('[name=csrfmiddlewaretoken]').value;
        // console.log(csrftoken)
        console.log(csrfTokenValue)
        // openChargebeeCheckout(selectedPriceId, csrftoken)
        openChargebeeCheckout(selectedPriceId, csrfTokenValue)
    })
    function openChargebeeCheckout(priceId, csrftoken) {
        cbInstance.openCheckout({
            hostedPage: function () {
                return $.ajax({
                    headers: { 'X-CSRFToken': csrftoken },
                    method: 'POST',
                    mode: 'same-origin',
                    url: '/api/generate_checkout_new_url', // url point to "CreateCBSubscriptionView"
                    data: {
                        price_id: priceId,
                        csrfmiddlewaretoken: csrftoken
                    },
                })
            },
            loaded: function () {
                console.log('checkout opened')
            },
            error: function () {
                $('#loader').hide()
                $('#errorContainer').show()
            },
            close: function () {
                $('#loader').hide()
                $('#errorContainer').hide()
                console.log('checkout closed')
            },
            success: function (hostedPageId) {
                console.log(hostedPageId)
                // redirect to Any thank you page you want
                window.location.replace("http://localhost:8000/subscription"); // new
            },
            step: function (value) {
                console.log(value)
            },
        })
    }

    function getCookie(name) {
        let cookieValue = null
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';')
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim()
                if (cookie.substring(0, name.length + 1) === name + '=') {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
                    break
                }
            }
        }
        return cookieValue
    }
})